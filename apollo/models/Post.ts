import mongoose from 'mongoose'

const { Schema } = mongoose
mongoose.Promise = global.Promise

export interface IPost extends mongoose.Document {
  title: string;
  description: string;
  image: string;
  userId: string;
  createdAt?: string;
  email?: string;
  body?: string;
  type?: string;
  categories?: Array<string>;
  skills?: Array<string>;
}

const postSchema = new Schema({
  title: String,
  description: String,
  image: String,
  createdAt: String,
  email: String,
  userId: String,
  type: String,
  body: String,
  categories: [String],
  skills: [String],
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  }
});

module.exports = mongoose.models.Post || mongoose.model<IPost>('Post', postSchema);