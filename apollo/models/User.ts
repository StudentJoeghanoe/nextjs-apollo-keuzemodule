import mongoose from 'mongoose'

const { Schema } = mongoose
mongoose.Promise = global.Promise

export interface IUser extends mongoose.Document {
	id: string,
	email: string,
	password?: string,
  createdAt?: string,
}

const userSchema = new Schema({
  password: String,
  email: String,
  createdAt: String
});

module.exports = mongoose.models.User || mongoose.model<IUser>('User', userSchema);