import mongoose from "mongoose";

export default (async function connect(): Promise<void> {
	try {
		if(process.env.MONGO_DB_URI)
			await mongoose.connect(process.env.MONGO_DB_URI)
				.then(() => {
					console.log('connected to mongoDB database')
				})
				.catch((err) => {
					console.log('Could not connect to mongoDB -', err)
				});
	} catch (error) {
		console.error(error);
	}
})();