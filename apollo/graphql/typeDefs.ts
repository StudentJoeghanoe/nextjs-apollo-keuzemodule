import { gql } from "apollo-server-micro";

export default gql`
  scalar Upload

  type Post {
    id: ID!
    title: String!
    createdAt: String
    email: String
    description: String
    image: String
    userId: ID!
    body: String
    type: String
    categories: [String]
    skills: [String]
  }

  type User {
    id: ID!
    email: String!
    token: String!
    createdAt: String!
  }

  input RegisterInput {
    password: String!
    confirmPassword: String!
    email: String!
  }

  input PostInput {
    postId: String!
    title: String!
    description: String!
    image: String
    body: String
    type: String
    categories: [String]
    skills: [String]
  }

  type File {
    id: ID!
    filename: String!
    mimetype: String!
    userId: String!
    path: String!
  }

  type Query {
    hello: String
    doAuth: User
    getFiles: [File!]
    getPosts(isAdmin: Boolean): [Post]
    getUsers: [User]
    getPost(postId: ID!): Post
    getPostBySlug(slug: String!): Post
  }

  type Mutation {
    uploadFile(file: Upload!): File
    deleteFile(fileId: ID!): String!
    register(registerInput: RegisterInput): User!
    login(email: String!, password: String!): User!
    createPost(title: String!, description: String!, image: String): Post!
    deletePost(postId: ID!): String!
    updatePost(postInput: PostInput): Post!
  }
`;
