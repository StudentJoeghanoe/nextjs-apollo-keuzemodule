const { AuthenticationError } = require('apollo-server-micro');

const Post = require('../../models/Post');
import doAuth, { IRequestHeaders } from '../../util/doAuth';

export default {
  Query: {
    async getPosts(_: any, {
      isAdmin
    }: {
      isAdmin?: boolean
    }, context: IRequestHeaders) {
      if (isAdmin) {
        const user = doAuth(context);
        if (!user) {
          throw new Error('Could not verify user')
        }

        try {
          const posts = await Post.find().sort({ createdAt: -1 });
          return posts;
        } catch (err: any) {
          throw new Error(err);
        }
      };

      try {
        const posts = await Post.find().sort({ createdAt: -1 });
        return posts;
      } catch (err: any) {
        throw new Error(err);
      }
    },
    async getPost(_: any, {
      postId
    }: {
      postId: string
    }) {
      try {
        const post = await Post.findById(postId);
        if (post) {
          return post;
        } else {
          throw new Error('Post not found');
        }
      } catch (err: any) {
        throw new Error(err);
      }
    },
    async getPostBySlug(_: any, {
      slug
    }: {
      slug: string
    }) {
      try {
        const post = await Post.findOne({ title: slug });

        if (post) {
          return post;
        } else {
          throw new Error('Post not found');
        }
      } catch (err: any) {
        throw new Error(err);
      }
    }
  },
  Mutation: {
    async updatePost(_: any, {
      postInput
    }: {
      postInput: {
        title: string,
        description: string,
        image: string,
        postId: string,
        type: string,
        categories: Array<string>
        skills: Array<string>
        body: JSON
      }
    }, context: IRequestHeaders) {
      const user = doAuth(context);
      const { title, description, image, postId, body, type, categories, skills } = postInput;
      const post = await Post.findById(postId);

      if (user.id === post.userId) {
        await post.updateOne({
          title: title.toLowerCase(),
          description: description,
          type: type,
          image: image,
          categories: categories,
          skills: skills,
          body: body
        })

        return post
      }

      throw new Error('You are not allowed to edit this post.');
    },
    async createPost(_: any, {
      title,
      description,
      image
    }: {
      title: string,
      description: string,
      image: string
    }, context: IRequestHeaders) {
      const user = doAuth(context);

      if (title.trim() === '') {
        throw new Error('Post body must not be empty');
      }

      const newPost = new Post({
        title: title.toLowerCase(),
        description,
        image,
        userId: user.id,
        email: user.email,
        createdAt: new Date().toISOString()
      });

      const post = await newPost.save();

      return post;
    },
    async deletePost(_: any, {
      postId
    }: {
      postId: string
    }, context: IRequestHeaders) {
      const user = doAuth(context);

      try {
        const post = await Post.findById(postId);
        if (user.id === post.userId) {
          await post.delete();
          return 'Post deleted successfully';
        } else {
          throw new AuthenticationError('Action not allowed');
        }
      } catch (err: any) {
        throw new Error(err);
      }
    },
  }
};