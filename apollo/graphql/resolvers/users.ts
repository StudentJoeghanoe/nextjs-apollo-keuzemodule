import doAuth, { IRequestHeaders } from "../../util/doAuth";

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { UserInputError } = require('apollo-server-micro');

const {
	validateRegisterInput,
	validateLoginInput
} = require('../../util/validators');

const User = require('../../models/User');

export interface IUser {
	id: string,
	email: string,
	password?: string
}

function generateToken(user: IUser) {
	return jwt.sign(
		{
			id: user.id,
			email: user.email,
		},
		process.env.SECRET_KEY,
		{ expiresIn: '3h' }
	);
}

export default {
	Query: {
		async getUsers() {
			try {
				const users = await User.find().sort({ createdAt: -1 });
				return users;
			} catch (err: any) {
				throw new Error(err);
			}
		},
		async doAuth(_: any, {}: any, context: IRequestHeaders){
			try {
				const user = doAuth(context)
				return user;
			} catch(err: any){
				throw new Error(err)
			}
		}
	},
	Mutation: {
		async login(_:any, { email, password }: IUser) {
			const { errors, valid } = validateLoginInput(email, password);

			if (!valid) {
				throw new UserInputError('Errors', { errors });
			}

			const user = await User.findOne({ email });

			if (!user) {
				errors.general = 'User not found';
				throw new UserInputError('User not found', { errors });
			}

			const match = await bcrypt.compare(password, user.password);
			if (!match) {
				errors.general = 'Wrong crendetials';
				throw new UserInputError('Wrong crendetials', { errors });
			}

			const token = generateToken(user);

			return {
				...user._doc,
				id: user._id,
				token
			};
		},
		async register(
			_: any,
			{
				registerInput: { email, password, confirmPassword }
			}: {
				registerInput:
				IUser & {
					confirmPassword: string
				}
			}
		) {
			// Validate user data
			const { valid, errors } = validateRegisterInput(
				email,
				password,
				confirmPassword
			);
			if (!valid) {
				throw new UserInputError('Errors', { errors });
			}

			// TODO: Make sure user doesnt already exist
			const user = await User.findOne({ email });

			if (user) {
				throw new UserInputError('This email is already in use.', {
					errors: {
						email: 'This email is already in use.'
					}
				});
			}
			// hash password and create an auth token
			password = await bcrypt.hash(password, 12);

			const newUser = new User({
				email,
				password,
				createdAt: new Date().toISOString()
			});

			const res = await newUser.save();

			const token = generateToken(res);

			return {
				...res._doc,
				id: res._id,
				token
			};
		}
	}
};