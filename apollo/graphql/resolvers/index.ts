import usersResolvers from './users'
import postsResolvers from './posts'

export default {
//   Post: {
//     likeCount: (parent) => parent.likes.length,
//     commentCount: (parent) => parent.comments.length
//   },
  Query: {
    ...usersResolvers.Query,
    ...postsResolvers.Query,
    // ...fileResolvers.Query,
  },
  Mutation: {
    // ...fileResolvers.Mutation,
    ...usersResolvers.Mutation,
    ...postsResolvers.Mutation,
  }
//   ,
//   Subscription: {
//     ...postsResolvers.Subscription
//   }
};