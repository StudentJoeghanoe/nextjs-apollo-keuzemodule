import Layout from "../../components/Layout"
import WorkCard, { IWorkItem } from "../../components/WorkCard"
import { gql } from '@apollo/client'

import styles from '../../styles/WorkIndex.module.css'
import { getApolloClient } from "../../utils/Apollo/apollo"
// @ts-ignore No types or this library
import Masonry, {ResponsiveMasonry} from "react-responsive-masonry"

interface IWork {
    posts: Array<IWorkItem>
    errors: any
}

export const setImage = (url?: string): string => {
    if (!url) return ''
    const proxy = 'http://localhost:3000/'
    if (url) {
        if(url[0] === '/'){
            url = url.substring(1);
        }
        if (url.includes(proxy)) return url
        return proxy + url
    }
    return proxy
}

const Work = ({ posts, errors }: IWork) => {
    return <Layout
        contentClass={styles.content}
    >
        <h1 className={styles.heading}>cases & <span className={styles.outline}>projects</span></h1>
        <h2 className={styles.subheading}>A couple of designs, applications, hours, experiences, hard work <br /> and personal development presented as projects and designs</h2>
        <ResponsiveMasonry columnsCountBreakPoints={{500: 1, 750: 2, 1100: 3}}>
            <Masonry gutter="24px">
                {posts?.map((item, index) =>
                    <WorkCard key={`work-card-${index}`} image={setImage(item.image)} title={item.title} description={item.description} />
                )}
            </Masonry>
        </ResponsiveMasonry>
    </Layout>
}

export async function getServerSideProps() {
    const client = getApolloClient(true)

    const { data } = await client.query({
        query: gql`
            {
                getPosts{
                    id
                    title
                    description
                    image
                }
            }
        `
    })

    return {
        props: {
            posts: data.getPosts,
        },
    }
}

export default Work