import { createElement, useMemo } from "react"
import { motion } from "framer-motion"
import Image from 'next/image'

import Layout from "../../components/Layout"
import Button from "../../components/Button"
import Meta from "../../components/Meta"

import styles from '../../styles/Work.module.css'

import Background from "../../icons/background"
import { getApolloClient } from "../../utils/Apollo/apollo"
import { gql, useQuery } from '@apollo/client'
import { setImage } from "."
import { IPostItems } from "../../utils/Providers/PostProvider"

export const config = {
    unstable_runtimeJS: false
}
interface IPost {
    title: string;
    image: string;
    description: string;
    body: string;
    categories: Array<string>;
    skills: Array<string>;
    createdAt: Date;
    type: string;
    id: string
}

const Work = ({
    title,
    image,
    description,
    body,
    categories,
    skills,
    createdAt,
    type,
    id,
}: IPost) => {
    const elementTypes: { [key: string]: string } = {
        'heading-1': 'h1',
        'heading-2': 'h2',
        'paragraph': 'p',
        'image': 'img',
    }

    const returnElement = (element: { type: string, value?: string, alt?: string }, index?: number) => {
        return createElement(element.type, {
            key: `${element.type}-${index ?? element.value}-${index}`,
            className: styles[`item-${element.type}`],
            ...(element.type === 'video' && { autoPlay: true, playsInline: true, controls: false, loop: true, muted: true }),
            ...(element.type === 'img' && { alt: element.alt, src: setImage(element.value) }),
            ...(element.type === 'p' && { children: element.value }),
            ...(element.type === 'h1' && { children: element.value }),
            ...(element.type === 'h2' && { children: element.value })
        })
    }

    const animate = (i: number, direction?: string) => ({
        initial: {
            opacity: 0,
            ...(direction && { [direction]: 60 })
        },
        visible: {
            opacity: 1,
            ...(direction && { [direction]: [0] }),
            transition: {
                duration: .9,
                ease: 'easeOut',
                delay: i * .1
            }
        },
        exit: {
            opacity: 0,
            transition: {
                duration: .6
            }
        }
    })

    const serializedBody: Array<IPostItems> = useMemo(() => {
        if (body) {
            return JSON.parse(body)
        }
        return undefined
    }, [body])

    return <Layout
        contentClass={styles.content}
    >
        <Meta pageName={title} />
        <header className={styles.intro}>
            <motion.h1 initial="initial" animate="visible" exit="exit" variants={animate(2)} className={styles.title}>{title}</motion.h1>
            <div style={{ position: 'relative', width: '100%' }}>
                <Background className={styles.background} />
                <motion.div className={styles.macbook} initial="initial" animate="visible" exit="exit" variants={animate(3)}>
                    {image && <Image loading="eager" priority={true} alt="macbook" src={setImage(image)} layout="fill" objectFit="contain" objectPosition="center" />}
                </motion.div>
            </div>
            <motion.p initial="initial" animate="visible" exit="exit" variants={animate(3)} className={styles.description}>{description}</motion.p>

            <section className={styles.info}>
                <div className={styles.info__column}>
                    <span className={styles.info__heading}>Type</span>
                    <span className={styles.info__item}>{type}</span>
                </div>
                {categories && <div className={styles.info__column}>
                    <span className={styles.info__heading}>Expertises</span>
                    {categories?.map((item, index) => <span key={`expertises-${index}`} className={styles.info__item}>{item}</span>)}
                </div>}
                {skills && <div className={styles.info__column}>
                    <span className={styles.info__heading}>Skills</span>
                    {skills?.map((item, index) => <span key={`skills-${index}`} className={styles.info__item}>{item}</span>)}
                </div>}
            </section>
        </header>
        <main className={styles.images}>
            {serializedBody?.map((item, index) => {
                const type: string = item.type === 'image-double' ? 'image' : item.type

                if(type === 'image-double'){
                    return item?.content?.map((contentItem, contentIndex) => {
                        return returnElement({ ...contentItem, type: elementTypes[type] }, contentIndex)
                    })
                }

                return returnElement({ value: item.value, type: elementTypes[type] }, index)
            })}
        </main>
        <section className={styles.footer}>
            {/* <span className={styles.indicator}><span className={styles.indicator__line}/>{String(index + 1).padStart(2, '0')}</span> */}
            {/* <Button className={styles.link} href={`/work/${nextTitle}`} label="Next Project" hasArrow /> */}
        </section>
    </Layout>
}

export async function getServerSideProps({ params }: any) {
    const client = getApolloClient(true);

    if (params.slug === 'undefined') {
        return {
            redirect: {
                destination: '/work',
                permanent: false,
            }
        }
    }

    const { data, error } = await client.query({
        query: gql`
            query getPostBySlug($slug: String!){
                getPostBySlug(slug: $slug){
                    title
                    image
                    description
                    body
                    createdAt
                    categories
                    skills
                    type
                }
            }
        `,
        variables: {
            slug: params.slug
        }
    })

    if (error) {
        return {
            redirect: {
                destination: '/work',
                permanent: false,
            }
        }
    }

    return {
        props: {
            ...data.getPostBySlug,
            id: params.slug
        },
    }
}

export default Work