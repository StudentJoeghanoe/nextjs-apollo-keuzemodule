import { gql, useMutation, useQuery } from "@apollo/client"

import DashboardLayout from "../../../components/dashboard/Layout/Layout";

import useAuth from "../../../utils/Hooks/useAuth";

import styles from '../../../styles/Dashboard.module.css'
import CustomInput from "../../../components/dashboard/CustomInput/CustomInput";
import { useState } from "react";
import CustomTextarea from "../../../components/dashboard/CustomTextarea/CustomTextarea";
import FuncButton from "../../../components/FuncButton";
import { useRouter } from "next/router";


const NewPost = () => {
    useAuth({})

    const [form, setForm] = useState({
        title: "",
        description: ""
    })

    const router = useRouter();

    const [createNewPost] = useMutation(NEW_POST_QUERY, {
        variables: {
            title: form.title,
            description: form.description
        },
        onCompleted: (data) => {
            router.push(`/admin/posts/${data.createPost.id}`)
            
        }
    })

    return <DashboardLayout
        contentClass={styles.content}
        HeadingContent={[
            <h1 key={`heading-1`}>New Post</h1>
        ]}
    >
        <CustomInput placeholder="Title" value={form.title} callBack={(e: string) => setForm({ ...form, title: e })} />
        <CustomTextarea placeholder="Description" value={form.description} callBack={(e: string) => setForm({ ...form, description: e })} />
        <FuncButton theme="green" shape="default" onClick={createNewPost}>Create New Post</FuncButton>
    </DashboardLayout>
}

const NEW_POST_QUERY = gql`
    mutation createPost(
        $description: String!
        $title: String!
    ){
        createPost(title: $title, description: $description){
            title
            description
            id
        }
    }
`

export default NewPost