import { useContext, useState } from "react";
import { gql } from "@apollo/client"

import DashboardLayout from "../../../components/dashboard/Layout/Layout";
import ContentSelector from "../../../components/dashboard/ContentSelector/ContentSelector";
import CustomInput from "../../../components/dashboard/CustomInput/CustomInput";
import CustomTextarea from "../../../components/dashboard/CustomTextarea/CustomTextarea";

import useAuth from "../../../utils/Hooks/useAuth";

import { getApolloClient } from "../../../utils/Apollo/apollo";
import PostBuilder from "../../../components/dashboard/PostBuilder/PostBuilder";
import PostProvider, { IPost, PostContext } from "../../../utils/Providers/PostProvider";

import styles from '../../../styles/DashboardPost.module.css'
import FuncButton from "../../../components/FuncButton";
import ListBuilder from "../../../components/dashboard/ListBuilder/ListBuilder";
import EditImage from "../../../components/dashboard/EditImage/EditImage";
import { setImage } from "../../work";

const Post = ({ 
    post, 
    id 
}: {
    post: IPost;
    id: string
}) => {
    return <PostProvider
        initialPost={post}
        id={id}
    >
        <PostContent />
    </PostProvider>
}

const PostContent = () => {
    
    // Authenticate user for this page
    useAuth({})   
    
    const { info: { title, description, image, type, categories, skills }, info, post, savePost, setInfo } = useContext(PostContext)


    return <DashboardLayout
        HeadingContent={[
            <h1 key={`heading-1`}><strong>Post: </strong>{title}</h1>,
            // <FuncButton theme="green" shape="default" onClick={savePost}>Save Post</FuncButton>
        ]}
        contentClass={styles.content}
    >   
        <ContentSelector 
            className={styles.selector}
            actions={[
                <FuncButton key={`preview-button`} theme="transparent" shape="default" onClick={`/work/${title}`}>Preview</FuncButton>
            ]}
            content={[
                {
                    title: 'Body',
                    children: <PostBuilder postContent={post}/>
                },
                {
                    title: 'Info',
                    children: <div className={styles.infoContent}>
                        <div className={styles.input}>
                            <label className={styles.label} htmlFor="title">Title</label>
                            <CustomInput callBackOnChange callBack={(e: string) => setInfo({...info, title: e})} className={styles.input__field} name="title" value={title}/>
                        </div>

                        <div className={styles.textarea}>
                            <label className={styles.label} htmlFor="description">Description</label>
                            <CustomTextarea callBack={(e: string) => setInfo({...info, description: e})} className={styles.textarea__field} name="description" value={description}/>
                        </div>

                        <div className={styles.input}>
                            <label className={styles.label} htmlFor="type">Type</label>
                            <CustomInput callBackOnChange callBack={(e: string) => setInfo({...info, type: e})} className={styles.input__field} name="type" value={type}/>
                        </div>

                        <ListBuilder title="Categories" items={categories} callBack={(e) => setInfo({...info, categories: e})}/>

                        <ListBuilder title="Skills" items={skills} callBack={(e) => setInfo({...info, skills: e})}/>
                    </div>
                }
            ]}
        />
        <div className={styles.sidebar}>
            <FuncButton theme="green" shape="default" onClick={savePost}>Save</FuncButton>
            <div className={styles.sidebar__item}>
                <div className={styles.sidebar__header}>
                    <label className={styles.sidebar__heading}>Featured image</label>
                </div>
                <EditImage 
                    src={setImage(image)}
                    alt={title}
                    callBack={(e: string) => setInfo({...info, image: e})}
                />
            </div>
        </div>
    </DashboardLayout>
}

export async function getServerSideProps({ params }: any) {
    const client = getApolloClient(true);

    const { data, error } = await client.query({
        query: gql`
            query getPost($postId: ID!){
                getPost(postId: $postId){
                    title
                    image
                    description
                    body
                    createdAt
                    type
                    categories
                    skills
                }
            }
        `,
        variables: {
            postId: params.slug
        }
    })

    if(error){
        return {
            redirect: {
                destination: '/admin/dashboard',
                permanent: false,
            }
        }
    }

    return {
        props: {
            post: data.getPost,
            id: params.slug
        },
    }
}

export default Post