import React, { useContext, useEffect, useState } from 'react'
import Image from 'next/image'
import Layout from '../../../components/dashboard/Layout/Layout'

import styles from '../../../styles/DashboardImages.module.css'
import ContentSelector from '../../../components/dashboard/ContentSelector/ContentSelector'
import WithPreviews from '../../../components/dashboard/UploadWithPreview/uploadWithPreview'
import Delete from '../../../icons/Delete'
import { setImage } from '../../work'
import { AuthContext } from '../../../utils/Providers/AuthProvider'
import FuncButton from '../../../components/FuncButton'

const Images = ({ files }: any) => {
	const { user } = useContext(AuthContext)
	const [selected, setSelected] = useState<string | undefined>()

	const [images, setImages] = useState<Array<any>>(files)

	const getFiles = async () => {
		await fetch("http://localhost:3000/api/files", { method: "GET" })
			.then(response => response.json())
			.then(data => {
				setImages(data)
			});
	}
	
	const deleteImage = async () => {
		await fetch("/api/delete", { method: "POST", body: JSON.stringify({
			path: selected
		})})
		.then(response => response.json())
		.then(data => {
			getFiles();
		});

	}

	return <Layout
		HeadingContent={[
			<h1 key={`heading-1`}>Images</h1>
		]}
		contentClass={styles.content}
	>
		<ContentSelector
			actions={[
				<FuncButton key={`delete-button`} shape="icon" theme="red" isDisabled={!selected} onClick={deleteImage}><Delete /></FuncButton>
			]}
			content={[
				{
					title: 'Images',
					children:
						images?.length
							? <div className={styles.images}>
								{images.map((image: any, index: number) => {
									return <div
										onClick={() => setSelected(selected === image ? undefined : image)}
										className={`
										${styles.image}
										${typeof selected === 'string' ? selected !== image ? styles.not__selected : styles.selected : ''}
									`}
										key={`img-${index}`}
									>
										<Image objectFit="cover" layout="fill" src={setImage(image)} alt={image.filename}/>
									</div>
								})}
							</div>
							: <div className={styles.placeholder}>There are no images availible, upload some using the &quot;Add Image&quot; tab</div>
				},
				{
					title: 'Add Image',
					children: <WithPreviews refetch={getFiles} />
				}
			]}
		/>
	</Layout>
}

export async function getServerSideProps() {
	let files;

	await fetch("http://localhost:3000/api/files", {
		method: "GET",
	}).then(response => response.json())
		.then(data => {
			files = data
		});

	return {
		props: {
			files: files
		}
	}
}

export default Images