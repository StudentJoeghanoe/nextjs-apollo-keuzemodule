import { useMutation, gql } from '@apollo/client';
import { useRouter } from 'next/router';
import React, { ChangeEvent, useContext, useState } from 'react'

import Button from '../../components/Button'
import FuncButton from '../../components/FuncButton';
import { AuthContext } from '../../utils/Providers/AuthProvider';
import Layout from '../../components/Layout';

const Login = () => {
    const [errors, setErrors] = useState<{ [key: string]: string }>({})
    const [formValues, setFormValues] = useState({
        email: '',
        password: '',
    })

    const router = useRouter();
    const context = useContext(AuthContext);

    const [loginUser] = useMutation(LOGIN_USER, {
        update(
            _,
            {
                data: { login: userData }
            }
        ) {
            context.login(userData);
        },
        onError(err) {
            setErrors(err?.graphQLErrors[0].extensions?.exception.errors);
        },
        onCompleted() {
            router.push('/admin/dashboard');
        },
        variables: formValues
    });

    const submit = (e: ChangeEvent<HTMLFormElement>) => {
        e.preventDefault()
        loginUser()
    }

    const onChange = (e: ChangeEvent<HTMLInputElement>) =>
        setFormValues({ ...formValues, [e.target.name]: e.target.value.toLowerCase() })

    return <Layout>
        <div className="login__container">
            <form onSubmit={submit}
                className="login__content"
            >
                <h1 className="login__heading">Welkom Terug!</h1>

                <label className="login__input">
                    <input autoCapitalize="none" name="email" placeholder="Email" className="login__input-field" onChange={onChange} />
                </label>
                <label placeholder="password" className="login__input">
                    <input autoCapitalize="none" name="password" type="password" placeholder="Wachtwoord" className="login__input-field" onChange={onChange} />
                </label>

                <FuncButton theme="default" shape="default" isSubmit onClick={(e: Event) => e.stopPropagation()}><a>Log in</a></FuncButton>

                <ul>
                    {errors && Object.values(errors).map((value, index) => (
                        <li key={`error-${index}`}>{value}</li>
                    ))}
                </ul>
            </form>
        </div>
    </Layout>
}

const LOGIN_USER = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      email
      createdAt
      token
    }
  }
`;

export default Login