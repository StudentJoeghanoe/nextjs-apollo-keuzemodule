import { gql, useQuery } from "@apollo/client"

import DashboardLayout from "../../../components/dashboard/Layout/Layout";
import WorkCard, { IWorkItem } from "../../../components/WorkCard";

import useAuth from "../../../utils/Hooks/useAuth";

import styles from '../../../styles/Dashboard.module.css'
import { setImage } from "../../work";
//@ts-ignore No types for this library
import Masonry, {ResponsiveMasonry} from "react-responsive-masonry"
import { useState } from "react";
import FuncButton from "../../../components/FuncButton";

const Dashboard = () => {
    const { data, loading, error } = useQuery(GET_POSTS, {
        ssr: true,
        variables: {
            isAdmin: true
        },
    }) 

    const isAuthorized = useAuth({ error: error })  

    return <DashboardLayout
        isLoading={!!((loading || !!error) && !isAuthorized)}
        contentClass={styles.content}
        HeadingContent={[
            <h1 key={`heading-1`}>All posts</h1>,
            <FuncButton key={`heading-button`} shape="default" theme="green" onClick="/admin/posts/new">New Post</FuncButton>
        ]}
    >
        {data?.getPosts &&
            <ResponsiveMasonry columnsCountBreakPoints={{500: 1, 750: 2, 1100: 3, 1400: 4}}>
                <Masonry gutter="24px">
                    {data?.getPosts.map((item: IWorkItem, index: number) => {
                        return <WorkCard 
                            image={setImage(item.image)}
                            key={`card-${index}`}
                            title={item.title}
                            url={`/admin/posts/${item.id}`}
                            description={item.description}
                        />
                    })}
                </Masonry>
            </ResponsiveMasonry>}
    </DashboardLayout>
}

const GET_POSTS = gql`
    query getPosts($isAdmin: Boolean!){
        getPosts(isAdmin: $isAdmin){
            id
            title
            description
            image
        }
    }
`

export default Dashboard