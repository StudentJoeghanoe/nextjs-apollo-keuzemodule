import Link from 'next/link'
import Image from 'next/image'
import { motion } from 'framer-motion'

import Layout from '../components/Layout'
import HomeItem from '../components/HomeItem'
import SplitText from '../components/SplitText'
import Meta from '../components/Meta'

import styles from '../styles/Home.module.css'
import aboutImage from '../public/about-image.jpg'

import pageData from './pages.json'

export const config = {
    unstable_runtimeJS: false
}

const Home = ({isMobile}: any) => {        
    return (
        <Layout
            contentClass={styles.content}
        >
            <>
                <Meta pageName="Home" />
                <section className={styles.intro}>
                    <span className={`${styles.ball} ${styles.ball__1}`} />
                    <motion.span initial={{ opacity: 0 }} animate={{ opacity: 1, transition: { delay: .1 } }} className={styles.pre__heading}>Portfolio 2022</motion.span>
                    <motion.h1 className={styles.heading}><SplitText
                        initial={{ y: '100%' }}
                        animate="visible"
                        variants={{
                            visible: (i: number) => ({
                                y: 0,
                                transition: {
                                    delay: .2 + i * 0.1,
                                    duration: .8,
                                    ease: 'circOut'
                                }
                            })
                        }}>Pushing to realise innovative and creative solutions.</SplitText>
                    </motion.h1>
                </section>

                <section className={styles.about}>
                    <span className={`${styles.ball} ${styles.ball__2}`} />
                    <span className={`${styles.ball} ${styles.ball__3}`} />
                    <div className={styles.about__background} />
                    <Image src={aboutImage} alt="picture of me" className={styles.about__image} loading="eager"/>
                    <div className={styles.about__content}>
                        <p className={styles.about__paragraph}>I am Joeghanoe Bhatti (21) born and raised in The Netherlands. Currently I am a full time student software development and work as a freelance full-stack web developer. This portfolio is a showcase of my current skill level, concerning usability, performance, code quality and projects that I have completed for several clients</p>
                        <Link href="/about" scroll={false}><a className={styles.about__button}>Read More</a></Link>
                    </div>
                </section>

                <section className={styles.work}>
                    {pageData.map((item, index) => <HomeItem
                        isMobile={isMobile}
                        key={`home-item-${index}`} {...item}
                        isLast={pageData.length - 1 === index}
                        index={index}
                    />)}
                </section>
            </>
        </Layout>
    )
}

Home.getInitialProps = ({ req }: any) => {
    let userAgent;
    if (req) { // if you are on the server and you get a 'req' property from your context
        userAgent = req.headers['user-agent'] // get the user-agent from the headers
    } else {
        userAgent = navigator.userAgent // if you are on the client you can access the navigator from the window object
    }

    let isMobile = Boolean(userAgent.match(
        /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
    ))

    return { isMobile }
}

export default Home