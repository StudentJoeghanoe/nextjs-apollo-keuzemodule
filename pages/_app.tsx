import '../styles/globals.css'

import type { AppProps } from 'next/app'
import { AnimatePresence } from 'framer-motion'
import { useTransitionFix } from '../lib/UseTransitionFix'

import { AuthContext, AuthProvider } from '../utils/Providers/AuthProvider';
import { useContext } from 'react';
import { ApolloProvider } from '@apollo/client';
import { getApolloClient } from '../utils/Apollo/apollo';

function MyApp({ Component, pageProps, router }: AppProps) {
  const transitionCallback = useTransitionFix()
  const client = getApolloClient();

  return <AnimatePresence
    exitBeforeEnter
    onExitComplete={() => {
      transitionCallback();
      if (typeof window !== 'undefined') {
        window.scrollTo({ top: 0 })
      }
    }}
  >
    <ApolloProvider client={client}>
      <AuthProvider>
        <Component {...pageProps} key={router.asPath} />
      </AuthProvider>
    </ApolloProvider>
  </AnimatePresence>
}

export default MyApp
