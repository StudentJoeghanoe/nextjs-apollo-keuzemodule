import React from "react";

import Document from "next/document";
import { getDataFromTree } from "@apollo/react-ssr";
import { getApolloClient } from "../utils/Apollo/apollo";

// This function makes apolloState data availbile in the GetInitialProps state
class DocumentWithApollo extends Document {
    constructor(props: any){
        super(props);

        const { __NEXT_DATA__, apolloState } = props;
        __NEXT_DATA__.apolloState = apolloState;
    }

    static async getInitialProps(ctx: any){
        // Clear console to show any loggin outputs below
        // console.clear();

        // Start time to indicate time to render in ms
        const startTime = Date.now();

        // Force refresh of ApolloClient for SSR
        const apolloClient = getApolloClient(true)

        // Get the Data for SSR and pass it to the AppProps
        await getDataFromTree(<ctx.AppTree {...ctx.appProps} />);

        // Get the initialprops for the appContext
        const initialProps = await Document.getInitialProps(ctx);

        // Extract all data from the apolloClient and store it
        const apolloState = apolloClient.extract();

        console.info(`Render time: ${Date.now() - startTime} milliseconds.`)
        
        // Return the initialprops and apolloState
        return { ...initialProps, apolloState }
    }
}

export default DocumentWithApollo