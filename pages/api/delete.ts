import fs from 'fs'

const DeleteFile = (req: any, res: any) => {
    let { path } = JSON.parse(req.body);

    if (path[0] === '/') {
        path = path.substring(1);
    }

    try {
        fs.unlinkSync(`public/${path}`);

        res.status(201).send({ message: "Image deleted" });

    } catch (e: any) {
        res.status(400).send({ message: "Error deleting image!", error: e.toString(), req: req.body });
    }
}

export default DeleteFile