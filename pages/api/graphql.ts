import { ApolloServer } from "apollo-server-micro";
import { ApolloServerPluginLandingPageGraphQLPlayground } from "apollo-server-core";
import connect from "../../apollo/lib/db";
import typeDefs from '../../apollo/graphql/typeDefs'
import resolvers from '../../apollo/graphql/resolvers/index'

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }: any) => ({ req }),
  plugins: [
    ApolloServerPluginLandingPageGraphQLPlayground()
  ],
});

const startServer = apolloServer.start();

export default async function handler(req: any, res: any) {
  await connect.then(() => {
    console.log("Connected 🚀 To MongoDB Successfully");
  });
  
  await startServer;
  await apolloServer.createHandler({
    path: "/api/graphql",
  })(req, res);
}

export const config = {
  api: {
    bodyParser: false,
  },
};