import Formidable from "formidable";
import IncomingForm from "formidable/Formidable";
import shortid from "shortid";
const fs = require("fs");

export const config = {
    api: {
        bodyParser: false
    }
};

const uploadForm = (next: any) => (req: any, res: any) => {
    return new Promise(async (resolve, reject) => {
        try {
            const form: IncomingForm = new Formidable.IncomingForm({
                multiples: true,
                keepExtensions: true
            });
            form.once("error", console.error);
            form
                .on("fileBegin", (name, file) => {
                    console.log("start uploading: ", file.name);
                })
                // .on("aborted", () => console.log("Aborted..."));
            form.once("end", () => {
                console.log("Done!");
            });
            await form.parse(req, async (err, fields, files: any) => {
                if (err) {
                    throw String(JSON.stringify(err, null, 2));
                }
                const id = shortid.generate();
                console.log(
                    "moving file: ",
                    files.file.path,
                    " to ",
                    `public/images/${id}-${files.file.name}`
                );
                fs.renameSync(files.file.path, `public/images/${id}-${files.file.name}`);
                req.form = {
                    fields,
                    files
                };
                return resolve(next(req, res));
            });
        } catch (error) {
            return resolve(res.status(403).send(error));
        }
    });
};

function handler(req: any, res: any) {
    try {
        if (req.method === "POST") {
            res.status(200).send(req.form);
        } else {
            throw String("Method not allowed");
        }
    } catch (error) {
        res.status(400).json({
            message: JSON.stringify(error, null, 2)
        });
    }
}

export default uploadForm(handler);