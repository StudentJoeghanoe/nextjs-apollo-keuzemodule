import Image from 'next/image'
import { motion } from 'framer-motion'

import Layout from "../../components/Layout"

import styles from "../../styles/About.module.css"

import Triangle from '../../icons/outlined-triangle'

import AboutBG from "../../public/about/BG.jpg"
import AboutFG from "../../public/about/Foreground.png"
import StoryImage from "../../public/about/story.jpg"

const About = () => {
    return <Layout
        contentClass="about"
    >
        <motion.section initial={{ opacity: 0 }} animate={{ opacity: 1, transition: { duration: .5, delay: .3 } }} exit={{ opacity: 0 }} className={styles.header}>
            <div className={styles.header__image} />
            <h1 className={styles.header__heading}>About Me</h1>
        </motion.section>

        <motion.article transition={{ duration: .5 }} initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} className={styles.main}>
            <section className={styles.about}>
                <div className={styles.about__background} />
                <section className={styles.about__content}>
                    <section className={styles.about__section}>
                        <h2 className={styles.about__heading}>My story</h2>
                        <div className={styles.about__image_mobile}>
                            <Image alt="Picture of me" src={StoryImage} layout="fill" />
                        </div>
                        <p className={styles.about__paragraph}>Born in the Netherlands and in the age of technology I got my hands on my first laptop when I was 6 years old. Ever since then I have been fascinated by computer programming and computer graphic design.
                            <br /><br />In 2011 I got my hands on photoshop for the first time and discovered my passion for design. Starting 2015 I ventured out into development and I have been in love since day one.</p>
                    </section>
                    <section className={styles.about__section}>
                        <div className={styles.about__image}>
                            <Image priority={true} loading="eager" alt="Picture of me" src={AboutBG} layout="fill" />
                            <Triangle className={styles.about__triangle} />
                            <Image priority={true} loading="eager" alt="Cutout of me" src={AboutFG} layout="fill" objectFit="contain" objectPosition="bottom center" />
                        </div>
                    </section>
                </section>
            </section>

            <section className={styles.story}>
                <p className={styles.story__paragraph}>Now i work <i>freelance</i> as a <br /><i>full-stack Web Developer</i>. In my spare time I like to <i>cook</i> and <i>workout</i>.</p>
                <p className={styles.story__paragraph}>I enjoy building interactive, innovative and complex javascript based applications with frameworks like react and nextjs.</p>

                <section className={styles.links}>
                    <a className={styles.link} href="https://www.instagram.com/jo_bhti/" target="_blank" rel="noreferrer">Instagram</a>
                    <a className={styles.link} href="https://www.linkedin.com/in/joeghanoe/" target="_blank" rel="noreferrer">Linkedin</a>
                    <a className={styles.link} href="mailto:joeghanoe@gmail.com" target="_blank" rel="noreferrer">Joeghanoe@gmail.com</a>
                </section>
            </section>
        </motion.article>
    </Layout>
}

export default About