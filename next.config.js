const path = require('path')

module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['localhost', 'blob'],
  },
}
