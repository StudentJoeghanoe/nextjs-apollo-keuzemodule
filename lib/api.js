import pageData from '../pages/pages.json'

export const getPageData = (params) => {
    const getNextTitle = (i) => pageData[i + 1 > pageData.length - 1 ? 0 : i + 1]?.title

    return pageData.map((x, i) => ({
        ...x, 
        index: i, 
        nextTitle: getNextTitle(i).toLowerCase()
    })).find(x => x.title.toLowerCase() === params);
}

export async function getAllPostsWithSlug() {
    return pageData
}

export async function getFooterPosts(){
    return pageData.slice(1).slice(-5).map(item => ({title: item.title.toLowerCase()}))
}

export async function getWorkPagePosts(){
    const posts = pageData.map((item) => ({
        title: item.title,
        description: item.description,
        image: item.image
    }))

    return {
        posts: posts
    }
}