import React from 'react'

export default function ArrowUp() {
    return <svg width="1em" height="1em" fill="currentColor" version="1.1" id="Layer_1" viewBox="0 0 10.3 8.3">
        <g id="Group_105-up" transform="translate(0.15 0.189)">
        <path id="Path_19" d="M5.3,7.8l2.6-2.2C8.1,5.4,8.2,5.2,8,5C7.8,4.8,7.5,4.7,7.3,4.9L5.5,6.4V0.6
            c0-0.3-0.2-0.5-0.5-0.5c-0.3,0-0.5,0.2-0.5,0.5v5.8L2.7,4.9C2.5,4.7,2.2,4.8,2,4.9C1.8,5.1,1.9,5.4,2.1,5.6l2.6,2.2
            C4.9,7.9,5.1,7.9,5.3,7.8L5.3,7.8z"/>
        </g>
    </svg>
}