import React from 'react';

export default function Edit(){ 
    return <svg id="icons_Q2" fill="currentColor" width="1em" height="1em" viewBox="0 0 12.005 12.005">
        <path d="M15.82,8.429l-4.23-4.26a.57.57,0,0,0-.84,0l-6.57,6.57a.6.6,0,0,0-.18.42v4.23a.6.6,0,0,0,.6.6H8.83a.6.6,0,0,0,.42-.18l6.57-6.54a.57.57,0,0,0,0-.84Zm-7.23,6.36H5.2V11.4l3.75-3.72,3.36,3.39Zm4.59-4.56L9.79,6.839l1.38-1.41,3.39,3.42Z" transform="translate(-4 -3.985)"/>
    </svg>
}