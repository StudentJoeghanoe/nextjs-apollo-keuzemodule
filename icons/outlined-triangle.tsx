const Triangle = ({ className }: any) => <svg className={className} x="0px" y="0px" width="100%" height="86.60271903%" viewBox="0 0 662 573.31" fill="currentColor">
    <path d="M549.42,65,331,443.31,112.58,65H549.42M662,0H0L331,573.31,662,0Z" />
</svg>

export default Triangle