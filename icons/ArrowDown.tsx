import React from 'react'

export default function ArrowDown() {
    return <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 10.3 8.3" width="1em" height="1em" fill="currentColor">
        <g id="Group_105" transform="translate(0.15 0.189)">
            <path id="Path_19" d="M4.7,0.4L2.2,2.5C2,2.6,2,2.9,2.1,3.1c0.2,0.2,0.4,0.2,0.6,0.1l1.8-1.5v5.6
                c0,0.2,0.2,0.4,0.4,0.4c0.2,0,0.4-0.2,0.4-0.4V1.7l1.8,1.5c0.2,0.1,0.5,0.1,0.6-0.1C8,2.9,8,2.6,7.8,2.5L5.3,0.4
                C5.1,0.2,4.9,0.2,4.7,0.4L4.7,0.4z"/>
        </g>
    </svg>
}
