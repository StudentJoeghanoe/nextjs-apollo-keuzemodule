import React from 'react'

export default function Plus() {
    return <svg version="1.1" id="Layer_1" x="0px" y="0px" width="1em" height="1em" viewBox="0 0 9.5 9.5" fill="currentColor">
        <g id="Group_107" transform="translate(-1308.75 -219.75)">
            <g id="Line_41">
                <path d="M1318.2,224.5c0,0.4-0.3,0.8-0.8,0.8h-3.2v3.2c0,0.4-0.3,0.8-0.8,0.8s-0.8-0.3-0.8-0.8v-3.2h-3.2
                    c-0.4,0-0.8-0.3-0.8-0.8s0.3-0.8,0.8-0.8h3.2v-3.2c0-0.4,0.3-0.8,0.8-0.8s0.8,0.3,0.8,0.8v3.2h3.2
                    C1317.9,223.8,1318.2,224.1,1318.2,224.5z"/>
            </g>
        </g>
    </svg>
}
