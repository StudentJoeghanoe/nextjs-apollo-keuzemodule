## Running the client

To run the local development server:
```bash
$ yarn dev
```

## Running the server

To run the local development server:
```bash
$ yarn dev
```

use the debug dev line to run the debbuger alongside the development server
```bash
$ yarn dev:debug
```