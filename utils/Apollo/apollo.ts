import {
    ApolloClient,
    HttpLink,
    InMemoryCache,
    ApolloProvider,
    NormalizedCacheObject
} from "@apollo/client";
import { APOLLO_ENDPOINT } from "./config";
import { setContext } from "@apollo/client/link/context";

const isServer = typeof window === "undefined"
//@ts-ignore apolloState is a customAttribute on window object
const windowApolloState = !isServer && window.__NEXT_DATA__ .apolloState

let CLIENT: ApolloClient<NormalizedCacheObject>;

const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('devwise');

    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : ""
        }
    }
});

const client = new ApolloClient({
    uri: APOLLO_ENDPOINT,
    cache: new InMemoryCache(),
});

export default client

export function getApolloClient(forceNew?: boolean){
    if(!CLIENT || forceNew){
        CLIENT = new ApolloClient({
            uri: APOLLO_ENDPOINT,
            ...(!isServer && {
                ssrMode: isServer,
                link: authLink.concat(new HttpLink({ uri: APOLLO_ENDPOINT }))
            }),
            cache: new InMemoryCache(),
            // Default options to disable SSR for all queries
          
        })
    }
    
    return CLIENT
}