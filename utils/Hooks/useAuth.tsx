import { ApolloError, gql } from "@apollo/client"
import Router from "next/router"
import { useContext, useEffect } from "react"
import { AuthContext } from "../Providers/AuthProvider"

const useAuth = ({
    redirect,
    error
}: {
    redirect?: string;
    error?: ApolloError;
}) => {
    const { user } = useContext(AuthContext);
    
    useEffect(() => {
        const { pathname } = Router
        if(!user && pathname || pathname && error){
            Router.push(redirect ?? '/admin/login')
        }
    }, [user])

    return true
}

export default useAuth