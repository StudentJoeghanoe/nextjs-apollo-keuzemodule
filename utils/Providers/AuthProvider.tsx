import React, { useReducer, createContext } from 'react';
import jwtDecode, { JwtPayload } from 'jwt-decode';

interface IInitialState {
    user: null | object;
}

interface IUserData {
    token: string;
    password: string;
    email: string;
}

interface IAction {
    type: string;
    payload?: any
}

const initialState: IInitialState = {
    user: null
};

if(typeof window !== 'undefined'){
    const token: string | null = localStorage.getItem('devwise')
    
    if (typeof token === 'string') {
        const decodedToken: { exp: number } = jwtDecode(token);
    
        if (decodedToken.exp * 1000 < Date.now()) {
            localStorage.removeItem('devwise');
        } else {
            initialState.user = decodedToken;
        }
    }
}

const AuthContext = createContext({
    user: {
        id: undefined,
    },
    login: (userData: IUserData) => { },
    logout: () => { }
});

function authReducer(state: IInitialState, action: IAction) {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                user: action.payload
            };
        case 'LOGOUT':
            return {
                ...state,
                user: null
            };
        default:
            return state;
    }
}

function AuthProvider(props: any) {
    const [state, dispatch] = useReducer(authReducer, initialState);

    function login(userData: IUserData) {
        localStorage.setItem('devwise', userData.token);
        dispatch({
            type: 'LOGIN',
            payload: userData
        });
    }

    function logout() {
        localStorage.removeItem('devwise');
        dispatch({ type: 'LOGOUT' });
    }

    return (
        <AuthContext.Provider
            value={{ user: state.user, login, logout }}
            {...props}
        />
    );
}



export { AuthContext, AuthProvider };