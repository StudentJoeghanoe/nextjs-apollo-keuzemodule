import { IPostItems } from "./index"

const newField = (): IPostItems => ({
    value: '',
    type: 'paragraph',
    content: []
})

export default function editPost(
    value: string,
    type: string,
    column: number,
    valueType: string,
    post: Array<IPostItems>,
    setPost: Function
){
    const moveInArray = (array: Array<any>, moveUp: boolean, index: number) => {
        array.splice(index + (moveUp ? -1 : 1), 0, ...array.splice(index, 1))
        return array
    }

    const editValue = () => {
        if (valueType) {
            return setPost(Object.assign([], post, {
                [column]: {
                    ...post[column],
                    [valueType]: value
                }
            }))
        }
    }

    const editType = () => {
        return setPost(Object.assign([], post, {
            [column]: { ...post[column], type: value }
        }))
    }

    const removeRow = () => {
        let content = [...post]
        content.splice(column, 1)
        return setPost(content)
    }

    const addRow = () => {
        post.splice(+column + 1, 0, newField())
        return setPost([...post])
    }

    const moveRow = () => {
        let newPost = moveInArray(post, !!value, +column)
        return setPost([...newPost])
    }

    const actions: { [key: string]: Function } = {
        'edit-value': editValue,
        'edit-type': editType,
        'remove-row': removeRow,
        'add-row': addRow,
        'move-row': moveRow
    }

    if (actions[type]) return actions[type]()

    console.warn('function does not exist')
}