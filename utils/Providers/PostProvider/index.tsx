import { gql, useMutation, useQuery } from "@apollo/client";
import React, { useState, useCallback, useMemo, useEffect } from "react";
import editPost from "./editPost";

interface PostProvider {
    children: JSX.Element | JSX.Element[];
    id: string;
    initialPost: IPost;
}

export interface IContextValue {
    info: IInfo
    post: Array<IPostItems>
    setPost(post: Array<IPostItems>): void
    setInfo(info: IInfo): void
    savePost(): void,
    editPost: Function
}

export interface IPostItems {
    type: Types,
    content: Array<IPostContent>
    value?: string
}

interface IPostContent {
    value: string
    alt?: string
}

interface IInfo {
    title?: string
    description?: string
    image?: string
    type?: string
    categories?: Array<string>
    skills?: Array<string>
}

interface EditPost {
    type: string
    column: number
    value: string
    valueType: string
}

export interface IPost {
    description: string;
    title: string;
    createdAt: string;
    image: string;
    body: string;
    type: string;
    categories: Array<string>;
    skills: Array<string>;
}

type Types = 'heading-1' | 'heading-2' | 'paragraph' | 'image' | 'image-double'

export const PostContext = React.createContext<IContextValue>({
    info: {},
    post: [],
    setPost: () => { },
    setInfo: () => { },
    editPost: () => { },
    savePost: () => { }
});

const defaultPost: Array<IPostItems> = [
    {
        type: 'heading-1',
        content: [
            {  value: '' }
        ]
    },
    {
        type: 'paragraph',
        content: [
            {  value: '' }
        ]
    }
]

export default function PostProvider({ children, initialPost, id }: PostProvider) {
    const [info, setInfo] = useState<IInfo>({})
    const [post, setPost] = useState<Array<IPostItems>>(defaultPost)

    useEffect(() => {
        const serializedBody = JSON.parse(initialPost.body);
        const { title, description, image, type, categories, skills } = initialPost
        
        setPost((serializedBody === null || serializedBody.length) ? serializedBody : defaultPost)
        setInfo({
            title: title ?? '',
            description: description ?? '',
            image: image ?? '',
            type: type ?? '',
            categories: categories.length <= 0 ? [""] : categories,
            skills: skills.length <= 0 ? [""] : skills,
        })
    }, [initialPost])

    const [savePost] = useMutation(UPDATE_POST_MUTATION, {
        variables: {
            id,
            ...info,
            body: JSON.stringify(post),
        },
    });

    const contextValue: IContextValue = {
        info: useMemo(() => info, [info]),
        post: useMemo(() => post, [post]),
        setPost: useCallback((post) => setPost(post), [setPost]),
        setInfo: useCallback((info) => setInfo(info), [setInfo]),
        savePost: useCallback(() => savePost(), [savePost]),
        editPost: useCallback((values: EditPost) => {
            const { value, column, type, valueType } = values
            return editPost(value, type, column, valueType, post, setPost)
        }, [editPost, post, setPost])
    };

    return (
        <PostContext.Provider value={contextValue}>
            {children}
        </PostContext.Provider>
    );
}

const UPDATE_POST_MUTATION = gql`
    mutation updatePost(
            $description: String!
            $title: String!
            $id: String!
            $image: String
            $body: String
            $type: String
            $categories: [String]
            $skills: [String]
        ){
        updatePost(postInput: {
            description: $description,
            title: $title,
            postId:  $id,
            image: $image,
            body: $body
            type: $type
            categories: $categories
            skills: $skills
        }){
            id
            title
            image
            description
            body
            type
            categories
            skills
        }
    }
`