import React, { ReactChild, ReactChildren } from 'react'
import Link from 'next/link'

import styles from './dashboard/Button/Button.module.css'

type Shape = 'default' | 'rounded' | 'icon' | 'small'

type Theme = 'default' | 'transparent' | 'red' | 'green'

interface IButtonProps {
	shape: Shape,
	theme: Theme,
	children?: ReactChild | ReactChildren,
	onClick?: string | object | Function,
	className?: string,
	isDisabled?: boolean,
	isStretched?: boolean,
	isSubmit?: boolean,
	style?: object
}

const FuncButton = ({ children, onClick, className, style, isDisabled, isSubmit, theme, shape}: IButtonProps) => {
	const isFunc = typeof onClick === 'function' || isSubmit
	const buttonClass = `
		${className ? className : ''}
		${styles.container}
		${styles[`theme--${theme ?? 'default'}`]}
		${styles[`shape--${shape ?? 'default'}`]}
		${isDisabled && styles.disabled}
	`
	return isFunc
		? <button
			// @ts-ignore
			onClick={() => !isDisabled && !isSubmit ? onClick() : {}}
			className={buttonClass}
			style={style}
			type={isSubmit ? 'submit' : 'button'}
		>{children}</button>
		: <Link href={typeof onClick === 'object' ? onClick : { pathname: onClick }}>
			<a 
				className={buttonClass}
				style={style}
			>{children}</a>
		</Link>
}

export default FuncButton;