import Head from 'next/head'
import { useRouter } from 'next/router'

interface IMetaProps {
    metaDesc?: string,
    opengraphImage?: string,
    pageName?: string,
}

const Meta = (props: IMetaProps) => {
    const router = useRouter()
    const dev = process.env.NODE_ENV !== 'production'
    const metaDesc = "Portfolio website of Joeghanoe Bhatti"

    const hostname = typeof window !== 'undefined' && !dev ? window.location.hostname : 'DevWise';

    return <Head>
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#000000" />
        <link rel="shortcut icon" href="/favicon.ico" />
        {!dev ? <base target="blank" href={hostname.toLowerCase()}/> : undefined}
        <meta name="msapplication-TileColor" content="#000000" />
        <meta name="msapplication-config" content="/browserconfig.xml" />
        <meta name="theme-color" content="#000" />
        <meta name="robots" content="all" />
        <link rel="alternate" type="application/rss+xml" href="/feed.xml" />
        <title>{hostname.includes('joeghanoe') ? 'Joeghanoe' : 'DevWise'}{props.pageName && ` | ${props.pageName}`}</title>
        <meta name="description" content={props?.metaDesc?.length ? props.metaDesc : metaDesc} />
        {props?.opengraphImage?.length && <meta property="og:image" content={props.opengraphImage} />}
        <meta property="og:description" content={props?.metaDesc?.length ? props.metaDesc : metaDesc} />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`${hostname.toLowerCase()}${router.asPath}`}/>
    </Head>
}

export default Meta