import { useEffect, useState } from 'react'
import Link from 'next/link'
import { motion } from 'framer-motion'

import styles from '../styles/Footer.module.css'

import { getFooterPosts } from '../lib/api'

const Footer = () => {
    const [posts, setPosts] = useState<any>([])

    useEffect(() => {
        new Promise((resolve, reject) => resolve(getFooterPosts()))
            .then(res => setPosts(res))
    }, [])

    return <footer className={styles.container}>
        <motion.h3 initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} className={styles.title}>
            <span className={styles.title__hover}>Get in touch,</span><br />
            <span className={styles.title__hover}>let&apos;s get to work!</span>
        </motion.h3>
        <motion.section initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} className={styles.content}>
            <section className={styles.info}>
                <span className={styles.heading}>Contact</span>
                <a className={styles.link} href="https://www.instagram.com/jo_bhti/" target="_blank" rel="noreferrer">Instagram</a>
                <a className={styles.link} href="https://www.linkedin.com/in/joeghanoe/" target="_blank" rel="noreferrer">Linkedin</a>
                <a className={styles.link} href="mailto:joeghanoe@gmail.com" target="_blank" rel="noreferrer">Joeghanoe@gmail.com</a>
            </section>

            <section className={styles.info}>
                <span className={styles.heading}>Also visit</span>
                <Link href="/" scroll={false}><a className={styles.internal__link}>Home</a></Link>
                <Link href="/work" scroll={false}><a className={styles.internal__link}>Work</a></Link>
                <Link href="/about" scroll={false}><a className={styles.internal__link}>About</a></Link>
            </section>

            <section className={styles.info}>
                <span className={styles.heading}>Posts</span>
                {posts.map((item: any, index: number) => 
                    <Link href={`/work/${item.title}`} key={`footer-link-${index}`} scroll={false}><a className={styles.internal__link}>{item.title}</a></Link>
                )}
                
            </section>
        </motion.section>
    </footer>
}

export default Footer