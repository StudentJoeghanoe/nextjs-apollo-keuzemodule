import Image from 'next/image'
import Link from 'next/link'

import styles from '../styles/WorkIndex.module.css'

export interface IWorkItem {
    title: string
    description: string
    image?: string
    url?: string
    id?: string
}

const WorkCard = ({image, title, description, url}: IWorkItem) => {
    return <Link passHref={true} href={url ? url : `/work/${title.toLowerCase()}`} scroll={false}>
        <section className={styles.card__container}>
            <footer className={styles.card__footer}>
                <h3 className={styles.card__heading}>{title[0].toUpperCase() + title.slice(1, title.length).toLowerCase()}</h3>
                <p className={styles.card__description}>{description}</p>
            </footer>
            <header className={styles.card__image__container}>
                {image 
                    ? <Image src={image} loading="lazy" className={styles.card__image} alt={title} layout="fill" objectFit="cover" objectPosition="center" />
                    : <div className={styles.card__image__placeholder}><h3>:&nbsp;&apos;&nbsp;)</h3></div>}
            </header>
        </section>
    </Link>
}

export default WorkCard