import React from 'react'
import { motion } from 'framer-motion'

const SplitText = ({ children, ...rest }: any) => {
  let words = children.split(' ')
  return words.map((word: string, i: number): any => {
    return (
      <span
        key={children + i}
        style={{ display: 'inline-block', overflow: 'hidden' }}
      >
        <motion.span
          {...rest}
          style={{ display: 'inline-block', willChange: 'transform' }}
          custom={i}
        >
          {word + (i !== words.length - 1 ? '\u00A0' : '')}
        </motion.span>
      </span>
    )
  })
}

export default SplitText
