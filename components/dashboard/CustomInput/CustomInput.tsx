import React, { useState, useEffect, useRef, memo, useCallback } from 'react'

import styles from './CustomInput.module.css'

interface IInputProps {
    value: string | number | undefined
    callBack(value: string): void
    name?: string
    className?: string
    placeholder?: string
    callBackOnChange?: boolean
    type?: string
    isDisabled?: boolean
    maxLength?: number
}

const CustomInput = ({ placeholder, value, name, callBack, className, callBackOnChange, type = 'text', isDisabled = true, maxLength = undefined }: IInputProps) => {
    const [textValue, setTextValue] = useState(value);

    const isValidInput = useRef(true)

    const classNames = `
        ${className ?? ''} 
        ${styles.container}
        ${styles[type]}
    `

    useEffect(() => {
        setTextValue(value)
    }, [value])

    const handleCallBack = (e: any) => callBack && callBack(e.target.value)

    const handleValue = (e: any) => (e.type === "blur" || e.key === "Enter") && handleCallBack(e)

    const handleChange = useCallback(
        (e) => {
            setTextValue(e.target.value)
        },
        [],
    )

    return <input
        type={type}
        onChange={(e) => {
            if (isValidInput.current) handleChange(e)
            if (callBackOnChange) handleCallBack(e)
        }}
        onBlur={e => handleValue(e)}
        onKeyPress={(e: any) => {
            if(e.keyCode === 8) {
                isValidInput.current = true
            }
            
            (e.keyCode || e.charCode === 13) && e.target.blur()
        }}
        value={textValue ?? ''}
        disabled={!isDisabled}
        maxLength={maxLength}
        name={name}
        id={name}
        className={classNames}
        placeholder={placeholder}
    />
}

export default memo(CustomInput)