import React, { memo } from "react";
import { setImage } from "../../../pages/work";
import ArrowDown from "../../../icons/ArrowDown";
import ArrowUp from "../../../icons/ArrowUp";
import Delete from "../../../icons/Delete";
import Plus from "../../../icons/Plus";
import Button from "../Button/Button";
import CustomInput from "../CustomInput/CustomInput";
import CustomTextarea from "../CustomTextarea/CustomTextarea";
import EditImage from "../EditImage/EditImage";

import styles from './BuilderItem.module.css'

interface IBuilderItem {
    type?: string
    index: number
    isLast: boolean
    removeItem(index: number): void
    addItem(index: number): void
    moveItem(index: number, direction: boolean): void
    callBack(e: string, index: number, type: string): void
}

interface IReturnItem {
    type?: string
    value?: string
    alt?: string
}

const BuilderItem = ({
    type, 
    index, 
    isLast, 
    callBack, 
    removeItem, 
    addItem, 
    moveItem, 
    ...props
}: IBuilderItem) => {
    const returnItem = ({type, ...props}: IReturnItem) => {
        if(type){
            const availibleItems: {[key: string]: JSX.Element} = {
                'heading-1': <CustomInput callBack={(e: string) => callBack(e, index, 'value')} className={styles.input} value={props.value}/>,
                'heading-2': <CustomInput callBack={(e: string) => callBack(e, index, 'value')} className={styles.input} value={props.value}/>,
                'paragraph': <CustomTextarea callBack={(e: string) => callBack(e, index, 'value')} className={styles.textarea} value={props.value}/>,
                'image': <>
                    <EditImage 
                        src={setImage(props.value)}
                        alt={props.alt}
                        className={styles.image}
                        callBack={(e: string) => callBack(e, index, 'value')}
                    />
                    <div className={styles.image__content}>
                        <span>Alt Tag </span>
                        <CustomInput callBack={(e: string) => callBack(e, index, 'alt')} value={props.alt}/>
                    </div>
                </>,
            }
    
            return availibleItems[type]
        }
    }

    const labels: {[key: string]: string} = {
        'heading-1': 'Heading (H1)',
        'heading-2': 'Heading (H2)',
        'paragraph': 'Paragraph',
        'image': 'Image',
        // 'image-double': 'Double Image',
    }

    return (
        <div className={styles.container}>
            <div className={styles.header}>
                <label className={styles.label}>{type && labels[type]}</label>
                <select onChange={(e) => callBack(e.target.value, index, 'type')} value={type} className={styles.select}>
                    <option hidden value="undefined">Kies een optie</option>
                    {Object.keys(labels).map((option, index) => 
                        <option key={`option-label-${index}`} value={option}>{labels[option]}</option>
                    )}
                </select>
                <div className={styles.buttons}>
                    <Button theme="red" shape="icon" onClick={() => removeItem(index)}><Delete /></Button>
                    <Button theme="default" shape="icon" onClick={() => moveItem(index, false)}><ArrowUp /></Button>
                    <Button theme="default" shape="icon" onClick={() => moveItem(index, true)}><ArrowDown /></Button>
                    <Button theme="green" shape="icon" onClick={() => addItem(index)}><Plus /></Button>
                </div>
            </div>
            <div className={`
                ${styles.content}
                ${styles[`content-${type}`]}
            `}>
                {returnItem({type: type === 'image-double' ? 'image' : type, ...props})}
            </div>
        </div>
    )
}

export default memo(BuilderItem)