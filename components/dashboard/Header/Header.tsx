import { useRouter } from 'next/router'
import React from 'react'
import { NavLink } from '../../NavLink'

import styles from './Header.module.css'

interface IHeader {
    className: string
}

const Header = ({className}: IHeader) => {
    let router = useRouter()

    return (
        <nav 
            className={`
                ${styles.container}
                ${className}
            `}
        >
            <div className={styles.content}>
                <div className={styles.logo} onClick={() => router.push('/admin/dashboard')}>
                    DevWise
                </div>
                <div className={styles.items}>
                    <div className={styles.label}>Content</div>
                    <NavLink activeClass={styles.item__active} className={styles.item} href="/admin/dashboard">Posts</NavLink>
                    <NavLink activeClass={styles.item__active} className={styles.item} href="/admin/images">Images</NavLink>
                    <div className={styles.label}>Website</div>
                    <NavLink className={styles.item} href="/">Home</NavLink>
                    <NavLink className={styles.item} href="/work">Work</NavLink>
                    <NavLink className={styles.item} href="/about">About</NavLink>
                </div>
            </div>
        </nav>
    )
}

export default Header