import React from 'react'
import styles from './PageHeading.module.css'

interface IPageHeading {
    headingContent?: JSX.Element[]
    className: string
}

const PageHeading = ({className, headingContent}: IPageHeading) => {
    return (
        <header
            className={`
                ${styles.container}
                ${className}
            `}
        >
            {headingContent && headingContent.map((content, index) => {
                return <React.Fragment key={`page-heading-content-${index}`}>{content}</React.Fragment>
            })}
        </header>
    )
}

export default PageHeading