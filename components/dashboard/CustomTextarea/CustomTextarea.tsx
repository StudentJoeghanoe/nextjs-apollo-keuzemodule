import React, { useState, useEffect, memo, useCallback } from 'react'

import styles from './CustomTextarea.module.css'

interface IInputProps {
    value: string | number | undefined
    callBack?: Function
    name?: string
    className?: string
    placeholder?: string
    callBackOnChange?: boolean
    isDisabled?: boolean
    maxLength?: number
    isPlaceholderDefault?: boolean
}

const CustomTextarea = ({ placeholder, value, name, callBack, className, callBackOnChange, isDisabled = true, maxLength = undefined, isPlaceholderDefault }: IInputProps) => {
    const [textValue, setTextValue] = useState(value);

    const classNames = `
        ${styles.container}
        ${className ?? ''} 
    `

    useEffect(() => {
        setTextValue(value)
    }, [value, placeholder])

    const handleCallBack = (e: any) => callBack && callBack(e.target.value)

    const handleValue = (e: any) => (e.type === "blur" || e.key === "Enter") && handleCallBack(e)

    const handleChange = useCallback(
        (e) => {
            setTextValue(e.target.value)
        },
        [],
    )

    return <textarea
        onChange={(e) => {
            handleChange(e)
            if (callBackOnChange) handleCallBack(e)
        }}
        onBlur={e => handleValue(e)}
        onKeyPress={(e: any) => {
            (e.keyCode || e.charCode === 13) && e.target.blur()
        }}
        className={classNames}
        value={textValue ?? ''}
        disabled={!isDisabled}
        maxLength={maxLength}
        name={name}
        placeholder={isPlaceholderDefault ? undefined : placeholder}
    />
}

export default memo(CustomTextarea)