import React, { Fragment } from 'react'
import { useState } from "react"

import styles from './ContentSelector.module.css'

interface IContentSelector {
    content: Array<IContent>
    actions?: JSX.Element[]
    className?: string
}

interface IContent {
    title: string
    children: JSX.Element | JSX.Element[]
}

const ContentSelector = ({ content, actions = [], className }: IContentSelector) => {
    const [selected, setSelected] = useState<number>(0)

    return <div className={`
        ${styles.container}
        ${className ?? ''}
    `}>
        <div className={styles.header}>
            <div className={styles.buttons}>
                {content.map((item, index) =>
                    <span
                        onClick={() => setSelected(index)}
                        key={`selector-button-${index}`}
                        className={`
                            ${styles.button}
                            ${index === selected ? styles.button__active : ''}
                        `}
                    >{item.title}</span>
                )}
            </div>
            {actions.length >= 1 && <div className={styles.actions}>
                {actions.map((item: JSX.Element, index: number) => {
                    return <Fragment key={`item-${index}`}>{item}</Fragment>
                })}
            </div>}
        </div>
        <div className={styles.content}>
            {content[selected].children}
        </div>
    </div>
}

export default ContentSelector