import React, { useContext, useEffect, useState } from 'react'
import Image from 'next/image'

import styles from './ImageOverlay.module.css'

import { AuthContext } from '../../../utils/Providers/AuthProvider';
import { setImage } from '../../../pages/work';

interface IImageOverlay {
    isActive: boolean
    callBack?: Function
    toggle: Function
}

const ImageOverlay = ({callBack, isActive, toggle}: IImageOverlay) => {
    const [images, setImages] = useState<Array<string>>()

    const getFiles = async () => {
		await fetch("/api/files", { method: "GET" })
			.then(response => response.json())
			.then(data => {
				setImages(data)
			});
	}

    useEffect(() => {
        getFiles();
    }, [])

    return (
        <div
            onClick={(e) => {
                e.stopPropagation();
                toggle()
            }}
            className={`
            ${styles.container}
            ${styles[isActive ? 'active' : 'inactive']}
        `}>
            <div className={styles.content} onClick={(e) => e.stopPropagation()}>
                {images?.map((image, index: number) => {
                    return <Image 
                        onClick={(e) => {
                            e.stopPropagation();
                            callBack && callBack(image)
                            toggle();
                        }} 
                        key={`image-${index}`}
                        width={300}
                        height={300}
                        objectFit="cover"
                        src={setImage(image)} 
                        alt={image} 
                        className={styles.image}
                    />
                })}
            </div>
        </div>
    )
}

export default ImageOverlay