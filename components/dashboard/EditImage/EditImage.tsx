import React, { useEffect, useRef, useState } from 'react'
import Edit from '../../../icons/Edit'
import { setImage } from '../../../pages/work'
import FuncButton from '../../FuncButton'
import ImageOverlay from '../ImageOverlay/ImageOverlay'

import styles from './EditImage.module.css'

interface IEditImage {
    src: string;
    alt?: string;
    className?: String;
    callBack?: Function;
}

const EditImage = ({src, alt, className, callBack}: IEditImage) => {
    const [isActive, setIsActive] = useState(false)
    const image = useRef<HTMLImageElement>(null)
    const [imageSrc, setImageSrc] = useState<string | undefined>(setImage(src));

    useEffect(() => {
        if(image.current)
            image.current.addEventListener('error', () => setImageSrc(undefined))

        return () => image.current?.removeEventListener('error', () => setImageSrc(undefined))
    }, [])    

    useEffect(() => {
        setImageSrc(setImage(src))
    }, [src])

    const toggle = () => setIsActive(!isActive)

    return (
        <>  
            {isActive && <ImageOverlay isActive={isActive} toggle={toggle} callBack={callBack}/>}
            <div className={`
                ${styles.container}
                ${className ?? ''}
            `}>
                <div className={styles[!!imageSrc ? 'active' : 'no-content']} onClick={() => !imageSrc && toggle()}>
                    {!!imageSrc
                        ? <img src={imageSrc} alt={alt} className={styles.image} ref={image}/>
                        : <p>Select image using edit button</p>}
                    
                </div>
                <FuncButton theme="default" shape="icon" className={styles.button} onClick={toggle}><Edit /></FuncButton>
            </div>
        </>
    )
}

export default EditImage