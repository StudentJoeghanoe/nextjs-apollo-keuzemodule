import useAuth from '../../../utils/Hooks/useAuth'
import Header from '../Header/Header'
import PageHeading from '../PageHeading/PageHeading'

import styles from './Layout.module.css'

interface IDashboardLayout {
    children: JSX.Element | JSX.Element[]
    HeadingContent?: JSX.Element[]
    contentClass?: string
    isLoading?: boolean
}

const DashboardLayout = ({HeadingContent, children, contentClass, isLoading}: IDashboardLayout) => {
    return (
        <div className={styles.container}>
            <style>{"#__next{overflow: unset;}"}</style>
            <Header className={styles.header}/>
            <PageHeading className={styles.page__heading} headingContent={HeadingContent} />
            <div className={`
                ${styles.content} 
                ${contentClass ? contentClass : ''}
                ${styles[isLoading ? 'content__loading' : 'content__loaded']}
            `}>
                {children}
            </div>
        </div>
    )
}

export default DashboardLayout