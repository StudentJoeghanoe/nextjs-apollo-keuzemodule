import React, { useContext } from 'react';
import { IPostItems, PostContext } from '../../../utils/Providers/PostProvider';
import BuilderItem from '../BuilderItem/BuilderItem';

interface IPostBuilder {
    postContent: Array<IPostItems>
}

const PostBuilder = ({postContent}: IPostBuilder) => {
    const { editPost } = useContext(PostContext)

    return <div>
        {postContent?.map((item, index) => <BuilderItem 
            callBack={(e, index, type) => {
                editPost({ value: e, column: index, type: 'edit-value', valueType: type })
            }} 
            removeItem={(index) => {
                editPost({ type: 'remove-row', column: index })               
            }}
            addItem={(index) => {
                editPost({ type: 'add-row', column: index })
            }}
            moveItem={(index, direction) => {
                editPost({ type: 'move-row', column: index, value: direction })
            }}
            index={index} 
            isLast={index === postContent.length - 1}
            key={`item-${index}`}
            {...item} 
        />)}
    </div>
}

export default PostBuilder