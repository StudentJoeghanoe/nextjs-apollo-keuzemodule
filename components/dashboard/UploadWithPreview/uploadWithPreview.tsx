import React, { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { gql, useMutation } from '@apollo/client';

import styles from './UploadWithPreview.module.css'
import Button from '../Button/Button';

interface IWithPreviews {
  refetch(): void
}

export default function WithPreviews({ refetch }: IWithPreviews) {
  const [file, setFile] = useState<any>({
    preview: ''
  });

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFile => {
      setFile(
        Object.assign(acceptedFile[0], {
          preview: URL.createObjectURL(acceptedFile[0]),
        })
      );
    }
  });

  useEffect(() => () => {
    URL.revokeObjectURL(file.preview)
  }, [file]);

  function onSubmit() {
    const body = new FormData();
    body.append('file', file)

    fetch("/api/upload", {
      method: "POST",
      body
    })
      .then(() => {
        refetch()
      })
  }

  return (
    <section className={styles.container}>
      <div className={`${styles.dropzone} ${isDragActive && styles.isActive}`} {...getRootProps()}>
        <input {...getInputProps()} />
        {isDragActive ? (
          <p>Drop the files here ...</p>
        ) : (
          <p>Drag &apos;n&apos; drop some files here, or click to select files</p>
        )}
      </div>
      <aside className={styles.preview}>
        <div className={styles.thumb}>
          <img src={file.preview} className={`
            ${styles.img}
            ${!!file?.preview ? styles.image__active : ''}
          `} alt={file.length && "img"} />
        </div>
        <Button
          shape="default"
          theme="default"
          className={styles.button}
          isDisabled={!file?.preview}
          onClick={onSubmit}
        >Upload</Button>
      </aside>
    </section>
  );
}

