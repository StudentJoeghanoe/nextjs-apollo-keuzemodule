import { useEffect, useState } from "react"
import Delete from "../../../icons/Delete"
import Plus from "../../../icons/Plus"
import FuncButton from "../../FuncButton"
import CustomInput from "../CustomInput/CustomInput"

import styles from './ListBuilder.module.css'

interface IListBuilder {
    items?: Array<string>
    title: string
    callBack(items: Array<string>): void
}

const ListBuilder = ({items, title, callBack}: IListBuilder) => {
    const [list, setList] = useState<Array<string>>(items ?? [""])

    useEffect(() => callBack(list), [list, callBack])

    const addRow = (index: number) => {
        const newList = list;
        newList.splice(index + 1, 0, "")
        setList([...newList])
    }

    const removeRow = (index: number) => {
        if(list.length <= 1) return console.error('Can not remove the last listitem')
        const newList = list
        newList.splice(index, 1);
        setList([...newList])
    }

    return <div className={styles.container}>
        <span>{title}</span>
        {list?.map((item: string, index: number) => {
            return <div key={`list-item-${index}`} className={styles.item}>
                <CustomInput value={item} placeholder="placeholder" callBack={(e) => setList(Object.assign([], list, {[index]: e}))} />
                <FuncButton className={styles.item__buttons} theme="green" shape="icon" onClick={() => addRow(index)}><Plus /></FuncButton>
                <FuncButton className={styles.item__buttons} isDisabled={list.length <= 1} theme="red" shape="icon" onClick={() => removeRow(index)}><Delete /></FuncButton>
            </div>
        })}
    </div>
}

export default ListBuilder