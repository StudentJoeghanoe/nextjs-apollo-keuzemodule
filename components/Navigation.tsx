import { motion } from 'framer-motion'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import { useContext, useEffect, useState } from 'react'

import styles from '../styles/Navigation.module.css'
import { AuthContext } from '../utils/Providers/AuthProvider'
import Hamburger from './Hamburger'
import { NavLink } from './NavLink'

interface IColors {
    [key: string]: string | undefined
}

const colorProps: IColors = {
    about: 'var(--white)',
}

const Navigation = () => {
    const router = useRouter()
    const { user } = useContext(AuthContext)
    const [isOpen, setIsOpen] = useState<boolean>(false)

    const currentColor = colorProps[router.pathname.substring(1)]

    return <motion.nav style={{color: currentColor}} className={styles.container}>
        <Link href="/"><a className={`${styles.logo} ${styles[isOpen ? 'logo-open' : 'logo-closed']}`}>DevWise - Joeghanoe Bhatti</a></Link>
        
        <div className={styles.links}>
            <NavLink href="/" exact className={styles.link} activeClass={styles.link__active}>Home</NavLink>
            <NavLink href="/work" exact className={styles.link} activeClass={styles.link__active}>Work</NavLink>
            <NavLink href="/about" exact className={styles.link} activeClass={styles.link__active}>About</NavLink>
            {user && <NavLink href="/admin/dashboard" exact className={styles.link} activeClass={styles.link__active}>Dashboard</NavLink>}
        </div>

        <div className={`${styles.mobile__menu} ${styles[isOpen ? 'mobile__menu-open' : 'mobile__menu-closed']}`}>
            <NavLink href="/" exact className={styles.link} onClick={() => setIsOpen(false)} activeClass={styles.link__active}>Home</NavLink>
            <NavLink href="/work" className={styles.link} onClick={() => setIsOpen(false)} activeClass={styles.link__active}>Work</NavLink>
            <NavLink href="/about" className={styles.link} onClick={() => setIsOpen(false)} activeClass={styles.link__active}>About</NavLink>
        </div>

        <Hamburger
            className={styles.hamburger} 
            colors={{
                open:'var(--white)',
                closed: currentColor ?? 'var(--black)'
            }}
            isOpen={isOpen} 
            callBack={(val: boolean) => setIsOpen(val)}
        />
    </motion.nav>
}

export default Navigation