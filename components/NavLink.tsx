import { useRouter } from 'next/router';
import Link from 'next/link';
import { MouseEventHandler } from 'react';

export { NavLink };

interface INavLinkProps {
    href: string,
    exact?: boolean,
    props?: any,
    children?: any,
    className?: string,
    activeClass?: string
    onClick?: MouseEventHandler
}

function NavLink({ href, exact, activeClass, className, children, onClick, ...props }: INavLinkProps) {
    const { pathname } = useRouter();
    const isActive = exact ? pathname === href : pathname.startsWith(href);

    return <Link href={href}>
        <a {...props} onClick={onClick} className={`${className} ${isActive ? activeClass ?? 'active' : ''}`}>{children}</a>
    </Link>
}