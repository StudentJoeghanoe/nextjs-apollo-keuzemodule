import { AnimatePresence, motion } from 'framer-motion'
import { useRouter } from 'next/dist/client/router'
import styles from '../styles/Layout.module.css'
import Footer from './Footer'
import Meta from './Meta'
import Navigation from './Navigation'

interface ILayoutProps {
    children: any,
    contentClass?: string,
    hasFooter?: boolean,
    isLoading?: boolean,
    hasNav?: boolean
}

const variants = {
    hidden: {
        opacity: 0
    },
    visible: {
        opacity: 1
    },
    exit: {
        opacity: 0,
        transition: {
            delay: .1,
            duration: .4
        }
    }
}

const Layout = ({ children, contentClass, hasFooter = true, hasNav = true, isLoading = false }: ILayoutProps) => {
    if(isLoading) return <div>Loading...</div>

    return <section className={styles.container}>
        <style>{"#__next{overflow: hidden;}"}</style>
        {hasNav && <Navigation />}
        <motion.main
            className={`${styles.content} ${contentClass ?? ''}`}
            initial="hidden"
            animate="visible"
            exit="exit"
            transition={{ duration: .6, delay: 0, ease: 'easeInOut' }}
            variants={variants}
        >{children}</motion.main>
        {hasFooter && <Footer />}
    </section>
}

export default Layout