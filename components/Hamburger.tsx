import { useMemo } from 'react'
import styles from '../styles/Hamburger.module.css'

interface IHamburger {
    isOpen: boolean
    callBack: Function
    className: string
    colors?: {
        open: string,
        closed: string
    }
}

const Hamburger = ({isOpen, callBack, className, colors}: IHamburger) => {
    const lineStyle = useMemo(() => ({
        backgroundColor: colors ? colors[isOpen ? 'open' : 'closed'] : undefined
    }), [colors, isOpen])

    return <div 
        className={`${styles.container} ${styles[isOpen ? 'open' : 'closed']} ${className ?? ''}`} 
        onClick={() => callBack(!isOpen)}
    >
        <span className={styles.line} style={lineStyle}/>
        <span className={styles.line} style={lineStyle}/>
    </div>
}

export default Hamburger