import { useRef, useEffect, useState } from 'react'
import { motion } from 'framer-motion'
import Link from 'next/link'
import Image from 'next/image'

import Button from './Button'

import styles from '../styles/HomeItem.module.css'

interface IHomeItemProps {
    isMobile: boolean,
    isLast: boolean,
    index: number,
    title: string,
    description: string,
    type: string,
    date: string,
    image: string
}

const HomeItem = (props: IHomeItemProps) => {
    const container = useRef<HTMLDivElement>(null)
    const [isFirstAnimation, setIsFirstAnimation] = useState(false)

    const moveImage = () => {
        if (!container.current) return;
        let rect = container.current.getBoundingClientRect();
        if(!isFirstAnimation && rect.y - (window.innerHeight * .5) < 0) setIsFirstAnimation(true)
    }

    useEffect(() => {
        if(window.top) window.top.addEventListener('scroll', moveImage)
        
        return () => {
            if(window.top) window.top.removeEventListener('scroll', moveImage)
        }
    }, [moveImage])

    const animate = (i: number, direction = 'y') => ({
        initial: {
            opacity: 0,
            [direction]: 60
        },
        visible: {
            opacity: isFirstAnimation ? 1 : 0,
            [direction]: isFirstAnimation ? 0 : 60,
            transition: {
                duration: .6,
                ease: 'circOut',
                delay: i * .1
            }
        },
        exit: {
            opacity: 0,
            transition: {
                duration: .6
            }
        }
    })

    return <Link passHref={true} href={`/work/${props.title.toLowerCase()}`} scroll={false}>
        <div ref={container}  className={styles.container}>
                <motion.div className={styles.content}>
                    <motion.h2 initial="initial" animate="visible" exit="exit" variants={animate(1)} className={styles.title}>{props.title.toLowerCase()}</motion.h2>
                    <motion.p initial="initial" animate="visible" exit="exit" variants={animate(2)} className={styles.description}> {props.description}</motion.p>
                    <motion.span initial="initial" animate="visible" exit="exit" variants={animate(3)} className={styles.info}>{props.type}<br />{props.date}</motion.span>
                    <motion.span initial="initial" animate="visible" exit="exit" variants={animate(4)}>
                        <Button className={styles.button} hasArrow href={`/work/${props.title.toLowerCase()}`} label="View project" />
                    </motion.span>
                    
                    <div className={styles.counter}>
                        <motion.span 
                            initial={{opacity: 0, scaleX: 0}} 
                            animate={{opacity: isFirstAnimation ? 1 : 0, scaleX: isFirstAnimation ? 1 : 0, transition: {duration: .6, ease: 'circOut', delay: .6}}}
                        className={styles.counter__line} />
                        <motion.span 
                            initial={{opacity: 0, x: 0}} 
                            animate={{opacity: isFirstAnimation ? 1 : 0, x: isFirstAnimation ? 0 : 10, transition: {duration: .6, ease: 'circOut', delay: .5}}}
                        >{String(props.index + 1).padStart(2, '0')}</motion.span>
                    </div>
                </motion.div>
            <div className={styles.frame}><div className={styles.frame__inner} /></div>
            <div className={styles.image}>
                {props.index === 0 && <div className={`${styles.frame} ${styles.frame__first}`} />}
                {props.isLast && <div className={`${styles.frame} ${styles.frame__last}`} />}
                <div className={styles.frame__image}><Image alt={`${props.title} main image`} loading="lazy" src={props.image} quality={50} objectFit="cover" layout="fill" /></div>
            </div>
        </div>
    </Link>
}

export default HomeItem