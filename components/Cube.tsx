import styles from '../../styles/Cube.module.css'

const Cube = () => {
    return <div className={styles.scene}>
        <div className={styles.container}>
        <div className={`${styles.face} ${styles.front}`}>front</div>
        <div className={`${styles.face} ${styles.back}`}>back</div>
        <div className={`${styles.face} ${styles.right}`}>right</div>
        <div className={`${styles.face} ${styles.left}`}>left</div>
        <div className={`${styles.face} ${styles.top}`}>top</div>
        <div className={`${styles.face} ${styles.bottom}`}>bottom</div>
        </div>
    </div>
}

export default Cube