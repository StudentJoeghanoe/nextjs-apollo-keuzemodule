import React from 'react'
import Link from 'next/link'

import styles from '../styles/Button.module.css'

interface IButtonProps {
    hasArrow?: boolean,
    className?: string,
    href: string,
    label: string,
    color?: string,
}

const Button = ({ hasArrow, className, href, label, color = "inherit" }: IButtonProps) => <Link href={href} scroll={false}>
    <a style={{ color: color }} className={`${styles.container} ${className}`}>
        {label} 
        {hasArrow && <span className={styles.icon}>-&gt;</span>}
    </a>
</Link>

export default Button


